module.exports = function(grunt) {

	const TARGET_DIRECTORY = '/home/myuser/code/node-projects/mysite-hexo';

	grunt.initConfig({
		gitpull: {
			site: {
				options: { cwd: TARGET_DIRECTORY }
			}
		}
		,exec: {
			hexoclean: {
				cwd: TARGET_DIRECTORY
				,cmd: 'hexo clean'
			}
			,hexogenerate: {
				cwd: TARGET_DIRECTORY
				,cmd: 'hexo generate'
			}
			,rsyncpublic: {
				cwd: TARGET_DIRECTORY
				,cmd: 'rsync -av --delete public/ /var/www/node/mysite/'
			}
		}
	});

	grunt.loadNpmTasks('grunt-git');
	grunt.loadNpmTasks('grunt-exec');
	grunt.registerTask('default', ['gitpull:site', 'exec:hexoclean', 'exec:hexogenerate','exec:rsyncpublic']);
}
