let express = require('express');
const path = require('path');
const { spawn } = require('child_process')
let router = express.Router();

const CONFIG = require('../config');
const DEFAULT_BUILD_DELAY = 300;
const EXECUTION_WATCHDOG_DELAY = 60*15;

const timers = {};
const executions = {};
const watchdogs = {};

function validateTrigger(conf, req){
	if (conf.method.toLowerCase() == 'gitlab'){
		if (conf.secret == req.get('X-Gitlab-Token')){
			return true;
		}
	}
	return false;
}

/* GET home page. */
router.get('/', function(req, res, next) {
	res.send('express-grunt-triggerer');
});

CONFIG.triggers.forEach( (trigger) => {
	let gruntfile = trigger.gruntfile;
	let immediateReturnConf = trigger.immediate_return;
	let buildDelay = trigger.build_delay || DEFAULT_BUILD_DELAY;
	let debug = trigger.debug;
	let basePath = trigger.base_path || path.dirname(gruntfile);
	let verbs = Array.isArray(trigger.verbs) ? trigger.verbs : [trigger.verbs];

	let callback = function(req, res, next) {
		if (trigger.validate && trigger.validate.enabled){
			if ( !validateTrigger(trigger.validate, req) ){
				return res.status(401).send('Unauthorized');
			}
		}

		const immediateReturn = immediateReturnConf && req.query.immediate_return !== '0';

		const watchdogFn = () => {
			console.log(`Watchdog for ${trigger.path}. Resetting execution to false`);
			executions[trigger.path] = false;
		}

		const buildFn = () => {
			if (watchdogs[trigger.path]) {
				clearTimeout(watchdogs[trigger.path]);
			}
			watchdogs[trigger.path] = setTimeout(watchdogFn, EXECUTION_WATCHDOG_DELAY * 1000);
			if (executions[trigger.path]) {
				res.json({ code: -1, status: 'ERROR', message: `Previous build is still in progress` });
				return;
			}
			executions[trigger.path] = true;
			//let process = spawn('grunt', ['--gruntfile', gruntfile, '-b', basePath], { cwd: basePath });
			let process = spawn('node_modules/grunt/bin/grunt', ['--gruntfile', gruntfile, '-b', basePath], { cwd: basePath });

			let result = [];
			process.stdout.on('data', (data) => { result.push( data.toString() ); });
			process.stderr.on('data', (data) => { result.push( data.toString() ); });
			process.on('exit', code => {
				executions[trigger.path] = false;
				clearTimeout(watchdogs[trigger.path]);
				console.log(`${trigger.path} - Exit code is: ${code}`);
				if (immediateReturn){
					result = null;
					return;
				}
				let output = { code };
				if (debug) {
					output.result = result;
				}
				res.json(output);
				result = null;
				output = null;
			});
		};

		if (timers[trigger.path]) {
			clearTimeout(timers[trigger.path]);
		}
		if (immediateReturn) {
			//we won't return so we can log the status
			res.json({ code: null, status: 'IMMEDIATE_RETURN', message: `Build will start in ${buildDelay} seconds` });

			timers[trigger.path] = setTimeout(buildFn, buildDelay * 1000);
		}
		else {
			setImmediate(buildFn);
		}
	}

	verbs.forEach( (verb) => {
		router[verb](trigger.path, callback);
	});
});

module.exports = router;

